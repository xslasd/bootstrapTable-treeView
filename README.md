# bootstrapTable-treeView 

#### 项目介绍
本人第一次上传项目，希望大家多多指教！

- 基于网上大神的代码，我更新了插件,
- 支持展开和闭合方法
- 支持叶子节点图标

### 优化

*  **优化ParentId判断规则，可以自定义ParentId名称。**
*  **优化Parent判断规则,可以自定义ParentId的值，顶级ParentId可以用使用null,直接填写数组中没有的ID就可以** 
*  **优化IE8显示，注意JSON格式标准化。**  

#### 样式
![输入图片说明](https://gitee.com/uploads/images/2018/0531/121514_55946188_1628448.png "样式.png")


#### 配置
```
var data = [
                {
                    id: '1',
                    name: "菜单一",
                    desc: "这是一个描述",
                    parentId:'123',
                },
                {
                    id: '2',
                    name: "菜单二",
                    desc: "这是一个描述",
                    parentId:'124',
                },
                {
                    id: '3',
                    name: "二级菜单1",
                    desc: "这是一个描述",
                    parentId:'1',
                },
                {
                    id: '4',
                    name: "二级菜单2",
                    desc: "这是一个描述",
                    parentId:'1',
                }
            ]
            $('#tree_table').bootstrapTable({
                data: data,
                sidePagination: 'server',
                pagination: false,
                treeView: true,
                treeId: "id",
                treeParentId:'parentId',
                collapseIcon: "glyphicon glyphicon-chevron-right",//折叠样式
                expandIcon: "glyphicon glyphicon-chevron-down",//展开样式
                leafIcon:"glyphicon glyphicon-th-list",//叶子节点样式
                treeField: "name",
                columns: [{
                    checkbox:true
                },{
                    field: 'name',
                    title: '名称',
                },
                {
                    field: 'desc',
                    title: '详情',
                },
                ]
            });
```
#### 图标参考地址
http://v3.bootcss.com/components/


